<?php
namespace requests;
use \config\config as config;  

class requests
{
	private static $cURL = null;
	private static $URL = null;
	private static $Auth = null;
	public function __construct()
	{

		$config = new config;
		
		self::$URL = $config->get('host');
		
		self::$Auth = $config->get('user') . ':' . $config->get('pass');

	}

	public static function PUT($extension, $args=null){
		self::$cURL = curl_init(self::$URL . $extension);
		curl_setopt(self::$cURL, CURLOPT_USERPWD, self::$Auth);
		curl_setopt(self::$cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(self::$cURL, CURLOPT_PUT, true);			
		curl_setopt(self::$cURL, CURLOPT_POSTFIELDS, $args);	
		$result = curl_exec(self::$cURL);
		$result = json_decode($result);
		return $result;
	}
	public static function DELETE($extension, $args=null){
		self::$cURL = curl_init(self::$URL . $extension);
		curl_setopt(self::$cURL, CURLOPT_USERPWD, self::$Auth);
		curl_setopt(self::$cURL, CURLOPT_RETURNTRANSFER, true);
		curl_setopt(self::$cURL, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt(self::$cURL, CURLOPT_POSTFIELDS, $args);	
		$result = curl_exec(self::$cURL);
		$result = json_decode($result);
		return $result;
	}
	public static function GET($extension, $args = null){
		
		self::$cURL = curl_init(self::$URL . $extension);
		
		curl_setopt(self::$cURL, CURLOPT_USERPWD, self::$Auth);

		curl_setopt(self::$cURL, CURLOPT_RETURNTRANSFER, true);

		curl_setopt(self::$cURL, CURLOPT_HTTPGET, true);

		isset($args) ? curl_setopt(self::$cURL, CURLOPT_POSTFIELDS, $args) : null; 

		$result = curl_exec(self::$cURL);

		curl_close(self::$cURL);
		
		$result = json_decode($result);
		
		
		return $result;
	}
	public static function POST($extension, $args = null){
		
		self::$cURL = curl_init(self::$URL . $extension);
		
		curl_setopt(self::$cURL, CURLOPT_USERPWD, self::$Auth);

		curl_setopt(self::$cURL, CURLOPT_RETURNTRANSFER, true);

		curl_setopt(self::$cURL, CURLOPT_HTTPGET, true);

		isset($args) ? curl_setopt(self::$cURL, CURLOPT_POSTFIELDS, $args) : null; 

		$result = curl_exec(self::$cURL);

		curl_close(self::$cURL);
		
		$result = json_decode($result);
		
		
		return $result;
	}
}
