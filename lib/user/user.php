<?php
namespace user;
use \requests\requests as requests;
class user
{
	protected static $request = null;
	
	private static $me = array();

	static function init()
	{
		self::$request = new requests;
		
	}
	
	# GET's
	public static function me() {
		
		$extension = "user/";
		return self::$request->GET($extension);
	}
	
	public static function privileges(){
		
		$extension = "user/privileges/";
		
		return self::$request->GET($extension);
	}
	
	public static function follows() {
		
		$extension = "user/follows/";
		
		return self::$request->GET($extension);
	}
	
	public static function repositories() {
		
		$extension = "user/repositories/";
		
		return self::$request->GET($extension);
	}
	
	public static function overview() {
		
		$extension = "user/repositories/overview/";
		
		return self::$request->GET($extension);
	}
	
	public static function dashboard() {
		
		$extension = "user/repositories/overview/";
		
		return self::$request->GET($extension);
	}
	public static function users() {
		
		$extension = "users/";
		
		return self::$request->GET($extension);
	}

	#Keys
	public static function keys()
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/ssh-keys/";
		return self::$request->GET($extension);
	}
	public static function getKey($keyId)
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/ssh-keys/".$keyId."/";
		return self::$request->GET($extension);
	}
	public static function delKey($keyId)
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/ssh-keys/".$keyId."/";
		return self::$request->DELETE($extension);
	}
	public static function editKey($keyId, $options)
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/ssh-keys/".$keyId."/";
		return self::$request->POST($extension, $options);
	}
	# Email functions.
	public static function emails()
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/emails/";
		return self::$request->GET($extension);
	}
	public function getEmail($email){
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/emails/".$email."/";
		return self::$request->GET($extension);
	}
	public function addEmail($email, $options=null)
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/emails/".$email;
		return isset($options) ? self::$request->PUT($extension, $options) :self::$request->PUT($extension);
	}
	public function delEmail($email)
	{
		$accountname = self::me()->user->username;
		$extension = "users/".$accountname."/emails/".$email;
		return self::$request->DELETE($extension);
	}
	public function primaryEmail($email)
	{
		$accountname = self::me()->user->username;
		$options = array('primary'=>true);
		$extension = "users/".$accountname."/emails/".$email;
		return self::$request->POST($extension, $options);
	}
}
user::init();